package br.imd.ufrn.shape;

public class ShapeFactory {

    public static Shape newShape() {
        return new Circle();
    }

    public static Shape newShape(int i) {
        switch (i) {
            case 1:
                return new Circle();
            case 2:
                return new Square();
            default:
                return null;
        }

    }
}

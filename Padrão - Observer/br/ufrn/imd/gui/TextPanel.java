package br.ufrn.imd.gui;

import br.ufrn.imd.TextObserver;
import br.ufrn.imd.WeatherForecast;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextPanel extends JPanel {

    public TextPanel() {
        JTextField txtTemperature = new JTextField(20);
        txtTemperature.setEnabled(false);
        add(txtTemperature);

        TextObserver observer = new TextObserver(txtTemperature);

        WeatherForecast.getInstance().registerObserver(observer);
    }
}

package br.ufrn.imd;

import java.util.LinkedHashSet;
import java.util.Set;

public class WeatherForecast implements Subject {

    public static WeatherForecast instance;

    private WeatherForecast() {
    }

    public static WeatherForecast getInstance() {
        if (instance == null) {
            instance = new WeatherForecast();
        }
        return instance;
    }

    private int temperatura;
    private Set<Observer> observers = new LinkedHashSet<>();

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
        notifyObserver();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer listagem : observers) {
            listagem.update(this);
        }
    }

}

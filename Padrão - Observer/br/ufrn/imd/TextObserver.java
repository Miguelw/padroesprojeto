package br.ufrn.imd;

import javax.swing.JTextField;

public class TextObserver implements Observer {

    private JTextField txtTemperatura;

    public TextObserver(JTextField txtTemperatura) {
        this.txtTemperatura = txtTemperatura;
    }

    @Override
    public void update(Subject subject) {
        WeatherForecast wf = (WeatherForecast) subject;
        this.txtTemperatura.setText(Integer.toString(wf.getTemperatura()));
        
    }

}

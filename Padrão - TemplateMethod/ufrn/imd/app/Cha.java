package ufrn.imd.app;

public class Cha extends Bebida {

    @Override
    public void adicionarAgua() {
        System.out.println("Agua Adicionada!");
    }

    @Override
    public void adicionarSubstancia() {
        System.out.println("Sabor de Cha adicionado!");
    }

    @Override
    public boolean levaAcucar() {
        return false;
    }

    @Override
    public void adicionarAcucar() {
        System.out.println("Acucar adicionado!");
    }

    @Override
    public void mexer() {
        System.out.println("Cha diluído!");
    }
}

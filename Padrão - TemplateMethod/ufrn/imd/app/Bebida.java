package ufrn.imd.app;

public abstract class Bebida {

    public void preparar() {
        adicionarAgua();
        adicionarSubstancia();
        if (levaAcucar()) {
            adicionarAcucar();
        }
        mexer();
    }

    public abstract void adicionarAgua();

    public abstract void adicionarSubstancia();

    public abstract boolean levaAcucar();

    public abstract void adicionarAcucar();

    public abstract void mexer();
}

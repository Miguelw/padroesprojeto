package ufrn.imd.app;

public class App {

    public static void main(String[] args) {
        Bebida suco = new Suco();
        Bebida cha = new Cha();
        suco.preparar();
        System.out.println("------------");
        cha.preparar();
    }

}

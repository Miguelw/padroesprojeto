package ufrn.imd.app;

public class Suco extends Bebida {

    @Override
    public void adicionarAgua() {
        System.out.println("Agua Adicionada!");
    }

    @Override
    public void adicionarSubstancia() {
        System.out.println("Sabor de suco adicionado!");
    }

    @Override
    public boolean levaAcucar() {
        return true;
    }

    @Override
    public void adicionarAcucar() {
        System.out.println("Acucar adicionado!");
    }

    @Override
    public void mexer() {
        System.out.println("Suco diluído!");
    }
}

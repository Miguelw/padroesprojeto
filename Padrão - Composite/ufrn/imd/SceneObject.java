package ufrn.imd;

public interface SceneObject {

    public void draw();
}

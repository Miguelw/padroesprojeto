package ufrn.imd;

import java.util.ArrayList;
import java.util.List;

public class SceneGroup implements SceneObject {

    private List<SceneObject> filho = new ArrayList<>();

    public void add(SceneObject scene) {
        filho.add(scene);
    }

    public void remover(SceneObject scene) {
        filho.remove(scene);
    }

    @Override
    public void draw() {
        for (SceneObject sceneObject : filho) {
            sceneObject.draw();
        }
    }

}

package ufrn.imd;

public class App {

    public static void main(String[] args) {
        SceneGroup grupo1 = new SceneGroup();
        Triangulo t1 = new Triangulo();

        SceneGroup grupo2 = new SceneGroup();

        grupo1.add(t1);
        grupo1.add(grupo2);

        Triangulo t2 = new Triangulo();
        Retangulo r1 = new Retangulo();

        grupo2.add(t2);
        grupo2.add(r1);

        SceneGroup grupo3 = new SceneGroup();

        Retangulo r2 = new Retangulo();
        Retangulo r3 = new Retangulo();
        
        grupo3.add(r2);
        grupo3.add(r3);
        
        grupo2.add(grupo3);
        
        grupo1.draw();

    }

}

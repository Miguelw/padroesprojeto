package ufrn.imd;

public class App {

    public static void main(String[] args) {
        //Sem adapter
        Temperatura f = new Temperatura();
        f.setValor(90);
        System.out.println("Temperatura em °F.: " + f.getValor());

        
        System.out.println("Convertendo valores");
        //Com adapter
        TemperaturaClassAdapter f2 = new TemperaturaClassAdapter();
        f2.setValor(90);
        System.out.println("Temperatura em °C.: " + f2.getValor());
        System.out.println("Referente em °F.: " + f2.getValueFahrenheit());
    }

}

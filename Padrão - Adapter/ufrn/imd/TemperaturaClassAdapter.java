package ufrn.imd;

public class TemperaturaClassAdapter extends Temperatura {

    //TRansformando °F em °C
    @Override
    public void setValor(double valor) {
        super.setValor(valor * 9 / 5 + 32);
    }

    @Override
    public double getValor() {
        return (super.getValor() - 32) * 5 / 9;
    }

    public double getValueFahrenheit() {
        return super.getValor();
    }
}

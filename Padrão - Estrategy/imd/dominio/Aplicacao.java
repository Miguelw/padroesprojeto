package imd.dominio;

import imd.Entidade.MovimentoAndar;
import imd.Entidade.MovimentoCorrer;
import imd.Entidade.Pessoa;

public class Aplicacao {

    public static void main(String[] args) {
        Pessoa p1 = new Pessoa("Miguel");
        p1.movimento(new MovimentoAndar(), 20);

        p1.movimento(new MovimentoCorrer(), 20);
    }

}

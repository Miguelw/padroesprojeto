package imd.Entidade;

public class MovimentoCorrer implements Movimento {

    @Override
    public void movimento(int distancia) {
        int distanciaAtual = 0;

        while (distanciaAtual < distancia) {
            distanciaAtual++;

            try {
                Thread.sleep(150);
            } catch (InterruptedException ex) {
            }

            System.out.println("Distancia Percorrida " + distanciaAtual + " m");
        }
    }

}

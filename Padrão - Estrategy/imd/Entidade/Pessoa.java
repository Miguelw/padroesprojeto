package imd.Entidade;

public class Pessoa {

    private String nome;

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public void movimento(Movimento movimento, int distancia) {
        System.out.println(nome + " Iniciou o movimento");
        movimento.movimento(distancia);
        System.out.println(nome + " Encerrou o movimento");
    }
}

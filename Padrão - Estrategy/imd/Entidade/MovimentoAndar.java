package imd.Entidade;

public class MovimentoAndar implements Movimento {

    @Override
    public void movimento(int distancia) {
        int distanciaAtual = 0;

        while (distanciaAtual < distancia) {
            distanciaAtual++;

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
            }

            if (distanciaAtual % 20 == 0) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ex) {
                }
                System.out.println("Parada para descanso");
            }
            System.out.println("Distancia Percorrida " + distanciaAtual + " m");
        }
    }

}
